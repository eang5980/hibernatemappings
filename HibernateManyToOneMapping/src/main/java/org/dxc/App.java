package org.dxc;


import org.dxc.entity.Address;
import org.dxc.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
	private static SessionFactory factory;
    public static void main( String[] args )
    {
       	try {
    			factory = new Configuration().configure().buildSessionFactory();
    		} catch (Throwable e) {
    			System.err.println("Failed to create Session Object " + e);
    			throw new ExceptionInInitializerError(e);
    		}
        	
        	Session session=factory.openSession();
        	Transaction tx = session.beginTransaction();
    	
    	Employee emp1=new Employee();    
    	emp1.setName("Edmund");    
    	emp1.setEmail("ed@gmail.com");    
          
        Employee emp2=new Employee();  
        emp2.setName("John");  
        emp2.setEmail("john@gmail.com");  
            
        Address address1=new Address();    
        address1.setAddressLine("Add123");    
        address1.setCity("City1");    
        address1.setState("state1");    
        address1.setCountry("country1");    
        address1.setPostalCode(123123);    
            
 
        emp1.setAddress(address1);
        emp2.setAddress(address1);
      
        session.persist(emp1);    
        session.persist(emp2);  
        tx.commit();    
    	
    	
    	session.close();
    	System.out.println("Success");
    }
}
