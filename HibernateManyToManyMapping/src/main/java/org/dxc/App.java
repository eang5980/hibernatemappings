package org.dxc;

import java.util.ArrayList;

import org.dxc.entity.Answer;
import org.dxc.entity.Question;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class App 
{
	private static SessionFactory factory;
    public static void main( String[] args )
    {
    	try {
			factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session Object " + e);
			throw new ExceptionInInitializerError(e);
		}
    	
    	Session session=factory.openSession();
    	Transaction tx = session.beginTransaction();
    	
    	Answer ans1 =  new Answer();
    	ans1.setAnswername("Java is a programming language");
    	ans1.setPostedBy("Jun Hao");
    	
    	Answer ans2 = new Answer();
    	ans2.setAnswername("Java is a platform");
    	ans2.setPostedBy("Brian");
    	
    	Answer ans3 = new Answer();
    	ans3.setAnswername("Servlet is an interface");
    	ans3.setPostedBy("Haikal");

    	Answer ans4 = new Answer();
    	ans4.setAnswername("Servlet is an API");
    	ans4.setPostedBy("Edmund");
    	
    	ArrayList<Answer> list1 = new ArrayList<Answer>();
    	list1.add(ans1); list1.add(ans2);
    	
    	ArrayList<Answer> list2 = new ArrayList<Answer>();
    	list1.add(ans3); list1.add(ans4);
    	
    	Question question1 = new Question();
    	question1.setQname("What is java?");
    	question1.setAnswer(list1);

    	Question question2 = new Question();
    	question2.setQname("What is a Servlet?");
    	question2.setAnswer(list2);

    	session.persist(question1);
    	session.persist(question2);
    	
    	tx.commit();
    	session.close();
    	System.out.println("Success");
    	
    }
    
}
